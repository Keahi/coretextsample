//
//  AttributedStringMetrics.swift
//  CoreTextSample
//
//  Created by Keahi Ertell on 7/6/20.
//  Copyright © 2020 Keahi Ertell. All rights reserved.
//

import Cocoa
import CoreText

public extension NSAttributedString {
	func drawingBounds(width: CGFloat) -> CGRect {
		let framesetter = CTFramesetterCreateWithAttributedString(self as CFAttributedString)
		let stringRange = CFRangeMake(0, length)
		
		// This path is a simple rectangle based off the width provided.
		// We give it an infinite height so that the framesetter can lay the text out as necessary.
		let path = CGPath(rect: CGRect(x: 0,
									   y: 0,
									   width: width,
									   height: CGFloat.greatestFiniteMagnitude),
						  transform: nil)
		
		// The framesetter lays out the text within the path we've specified.
		// This gives us a `CTFrameRef` which is able to calculate individual lines of text.
		let frame = CTFramesetterCreateFrame(framesetter,
											 stringRange,
											 path,
											 nil)
		
		// The `CTFrameRef` holds multiple lines of text and we can retrieve those as a `[CTLine]`.
		// From here, we'll want to get the bounds of each line so we can get the amount of space required by each line.
		let ctLines = CTFrameGetLines(frame)
		let lines = ctLines as! [CTLine]
		
		// Converts `CTLine` into `CGRect` bounds - the way we do this is important because it
		// determines the overall drawing bounds of the entire text.
		//
		// Here we use optical bounds as it seems to give the most accurate representation
		// of the text's actual bounds on the screen.
		let orderedLineBounds: [CGRect] = lines.map {
			CTLineGetBoundsWithOptions($0, [.useOpticalBounds])
		}
		
		// In the event we don't have any lines, there's no text bounds to speak of.
		guard !orderedLineBounds.isEmpty else { return .zero }
		
		// Each line rect is effectively zero based, so we need to offset each one to appear (vertically) after
		// the previous line's bounds. We do this, then union all line bounds together to get the final drawing bounds.
		let finalRect: CGRect = {
			var rect = orderedLineBounds[0]
			stride(from: 1, to: orderedLineBounds.count, by: 1).forEach { index in
				let currentRect = orderedLineBounds[index]
					.applying(CGAffineTransform(translationX: 0, y: rect.height))
				rect = rect.union(currentRect)
			}
			
			return rect
		}()
		
		// Return the sum of all line drawing bounds.
		return finalRect
	}
	
	func drawingHeight(for width: CGFloat) -> CGFloat {
		return drawingBounds(width: width).height
	}
}

public extension NSAttributedString {
	func fit(in frame: NSRect) -> (scaledString: NSAttributedString, scaleFactor: CGFloat, scaleCount: Int) {
		let tolerance: CGFloat = 1.0
		
		var minScaleFactor: CGFloat = 0.00001
		var maxScaleFactor: CGFloat = 100
		
		var currentScaleFactor: CGFloat = 1.0
		var currentScaledCopy = scaledCopy(to: currentScaleFactor)
		var currentHeight = currentScaledCopy.drawingHeight(for: frame.width)
		
		var scaleCount = 1
		
		while currentHeight > frame.height || abs(frame.height - currentHeight) > tolerance {
			let midpointScaleFactor = (minScaleFactor + maxScaleFactor) / 2.0
			guard midpointScaleFactor != currentScaleFactor else { break }
			
			currentScaleFactor = midpointScaleFactor
			currentScaledCopy = scaledCopy(to: currentScaleFactor)
			currentHeight = currentScaledCopy.drawingHeight(for: frame.width)
			
			scaleCount += 1
			
			if currentHeight > frame.height {
				maxScaleFactor = currentScaleFactor
			} else if currentHeight < frame.height {
				minScaleFactor = currentScaleFactor
			}
		}
		
		return (currentScaledCopy, currentScaleFactor, scaleCount)
	}
	
	func scaledCopy(to scaleFactor: CGFloat) -> NSAttributedString {
		let copy = NSMutableAttributedString(attributedString: self)
		
		copy.enumerateAttributes(in: copy.fullRange, options: []) { (attributes, range, stop) in
			guard let font = attributes[.font] as? NSFont else { return }
			let scaledFont = NSFontManager.shared.convert(font, toSize: font.pointSize * scaleFactor)
			copy.addAttribute(.font, value: scaledFont, range: range)
		}
		
		return copy
	}
	
	private var fullRange: NSRange {
		return NSRange(location: 0, length: length)
	}
}
