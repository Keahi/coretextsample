//
//  Convenience.swift
//  CoreTextSample
//
//  Created by Keahi Ertell on 7/7/20.
//  Copyright © 2020 Keahi Ertell. All rights reserved.
//

import Foundation

extension CGRect {
	var intDescription: String {
		return "\(Int(origin.x)) \(Int(origin.y)) \(Int(size.width)) \(Int(size.height))"
	}
}
