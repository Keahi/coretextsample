//
//  AppDelegate.swift
//  CoreTextSample
//
//  Created by Keahi Ertell on 7/6/20.
//  Copyright © 2020 Keahi Ertell. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
	
	@IBOutlet weak var window: NSWindow!
	
	func applicationDidFinishLaunching(_ aNotification: Notification) {
		window.contentView = CoreTextSampleView()
	}
}
