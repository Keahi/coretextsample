//
//  CoreTextSampleView.swift
//  CoreTextSample
//
//  Created by Keahi Ertell on 7/7/20.
//  Copyright © 2020 Keahi Ertell. All rights reserved.
//

import Cocoa

class CoreTextSampleView: NSView {
	
	let text = "Hello world this is a test!"
//	let text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut sollicitudin turpis, in consequat purus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed magna ligula, dignissim ullamcorper nisl ac, mattis imperdiet orci. Maecenas pretium egestas erat eget tincidunt. Praesent ullamcorper in odio quis interdum. Sed accumsan, nisi id dignissim imperdiet, massa eros condimentum massa, vel scelerisque nunc neque quis velit. Nunc finibus sapien nec est varius consequat. Proin ornare nisl at erat dignissim consectetur. Aliquam porttitor egestas risus non vehicula. Pellentesque blandit egestas tellus ut placerat. Ut dignissim, odio a hendrerit egestas, sapien tellus suscipit augue, sit amet pretium nunc mauris at lectus. Fusce eu leo turpis. Nunc cursus dapibus elit et ultricies. Pellentesque faucibus orci volutpat sem vestibulum cursus."
	
	override func draw(_ dirtyRect: NSRect) {
		let attributedString = NSAttributedString(string: text,
												  attributes: [
													.font: NSFont(name: "Helvetica Neue", size: 100)!,
//													.font: NSFont(name: "Mega Fresh", size: 100)!,
													.foregroundColor: NSColor.white,
		])
		
		let insetFrame = frame.insetBy(dx: 50, dy: 50)
		
		let scaledStringInfo = attributedString.fit(in: insetFrame)
		let scaledString = scaledStringInfo.scaledString
		
		print("Scaled to \(scaledStringInfo.scaleFactor) over \(scaledStringInfo.scaleCount) iterations")
		
		guard let context = NSGraphicsContext.current?.cgContext else { return }
		
		// Highlight the frame
		context.setStrokeColor(NSColor.orange.cgColor)
		context.stroke(frame)
		
		// Highlight the inset frame used for text
		context.setStrokeColor(NSColor.green.cgColor)
		context.stroke(insetFrame)
		
		// Draw the text within the given frame
		scaledString.draw(in: insetFrame)
	}
}
